import java.util.Scanner;

public class Weight{
	public static void main(String[] args){
		Scanner stdin = new Scanner(System.in);
		double kilogram;
		int pound;
		double ounce;
		System.out.printf("Enter weight in kilograms: ");
		kilogram = stdin.nextDouble();
		ounce = kilogram/0.02834952;
		pound = (int)(ounce/16);
		ounce -= (double)pound*16;
		System.out.printf("Equivalent imperial weight is %d lb %.1f oz", pound, ounce);
	}
}